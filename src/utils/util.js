/*
 * @Author: yangy
 * @Date: 2023-12-14 15:23:33
 * @LastEditors: yangy
 * @LastEditTime: 2024-01-12 15:51:00
 * @FilePath: /admin_vue3_element_plus/src/utils/util.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import { ElNotification, ElMessageBox } from "element-plus";
import nprogress from "nprogress";

export function Eptoast(
  message,
  type = "success",
  dangerouslyUseHTMLString = true,
  duration = 3000
) {
  ElNotification({
    message,
    type,
    dangerouslyUseHTMLString,
    duration,
  });
}

export function showModal(content = "提示内容", type = "warning", title = "") {
  return ElMessageBox.confirm(content, title, {
    confirmButtonText: "确定",
    cancelButtonText: "取消",
    type,
  });
}

// 显示全屏loading
export function showFullLoading() {
  nprogress.start();
}

// 隐藏全屏loading
export function hideFullLoading() {
  nprogress.done();
}

// 弹出输入框
export function showPrompt(tip, value = "") {
  return ElMessageBox.prompt(tip, "", {
    confirmButtonText: "确认",
    cancelButtonText: "取消",
    inputValue: value,
  });
}
