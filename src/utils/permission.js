/*
 * @Author: yangy
 * @Date: 2023-12-14 16:41:38
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-22 12:55:59
 * @FilePath: /admin_vue3_element_plus/src/utils/permission.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import { router, addRoutes } from "@/router";
import { getToken } from "./auth";
import { Eptoast, showFullLoading, hideFullLoading } from "./util";
import store from "@/store";

// 全局前置守卫

// 性能优化,因为每次点击左侧菜单栏切换的时候总是请求getInfo两次,所有定义这么一个变量
// 如果该变量为FALSE则未请求过可以请求,请求成功之后将该值设置为true 则下次不再请求
let hasGetInfo = false;
router.beforeEach(async (to, from, next) => {
  // 显示loading
  showFullLoading();
  const token = getToken();
  // 如果没有token 并且将要跳转的页面不是login 则强制跳转到login页面进行登录
  if (!token && to.path != "/login") {
    Eptoast("请登录", "error");
    return next({ path: "/login" });
  }
  // 防止重复登录, 有token值并且将要去的路径是login则 跳回到from处
  if (token && to.path == "/login") {
    Eptoast("您已登录", "error");
    return next({ path: from.path ? from.path : "/" });
  }
  // 如果用户登录,自动获取用户信息并存储在vuex当中
  let hasNewRoutes = false;
  if (token && !hasGetInfo) {
    let { menus } = await store.dispatch("getInfo");
    hasGetInfo = true;
    // console.log(menus);
    // 动态添加路由
    hasNewRoutes = addRoutes(menus);
  }

  // 设置页面标题
  let title = (to.meta.title ? to.meta.title : "") + "-商城后台管理系统";
  document.title = title;

  hasNewRoutes ? next(to.fullPath) : next();
});

// 全局后置守卫  用于全局加载完 隐藏进度条
router.afterEach((to, from) => hideFullLoading());
