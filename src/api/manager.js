/*
 * @Author: yangy
 * @Date: 2023-12-13 16:41:50
 * @LastEditors: yangy aidreams@sina.com
 * @LastEditTime: 2024-02-27 16:16:17
 * @FilePath: /admin_vue3_element_plus/src/api/manager.js
 * @Description: 管理员模块
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
// import axios from "@/axios";

import request from "@/utils/request";

// 登录
export const login = (username, password) => {
  return request("/admin/login", "post", { username, password });
};

// 获取用户信息
export const getInfo = () => {
  return request("/admin/getinfo", "post", {});
};

// 退出登录
export const logout = () => {
  return request("/admin/logout", "post", {});
};

// 修改密码
export const updatePassword = (data) => {
  return request("/admin/updatepassword", "post", data);
};

export function getManagerList(page, query = {}) {
  let q = [];
  for (let key in query) {
    if (query[key]) {
      q.push(`${key}=${encodeURIComponent(query[key])}`);
    }
  }
  let r = q.join("&");
  r = r ? "?" + r : "";
  return request(`/admin/manager/${page}${r}`, "get");
}
