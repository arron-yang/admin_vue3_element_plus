/*
 * @Author: yangy aidreams@sina.com
 * @Date: 2024-02-27 11:51:21
 * @LastEditors: yangy aidreams@sina.com
 * @LastEditTime: 2024-02-27 14:35:57
 * @FilePath: /admin_vue3_element_plus/src/api/notice.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from "@/utils/request";

export function getNoticeList(page) {
  return request(`/admin/notice/${page}`, "get");
}

// 增加公告
export function createNotice(data) {
  return request("/admin/notice", "post", data);
}

//更新
export function updateNotice(id, data) {
  return request(`/admin/notice/${id}`, "post", data);
}

//删除
export function deleteNotice(id) {
  return request(`/admin/notice/${id}/delete`, "post");
}
