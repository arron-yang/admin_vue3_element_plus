/*
 * @Author: yangy
 * @Date: 2024-01-02 16:30:06
 * @LastEditors: yangy
 * @LastEditTime: 2024-01-12 15:42:02
 * @FilePath: /admin_vue3_element_plus/src/api/image_class.js
 * @Description:
 *
 * Copyright (c) 2024 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import request from "@/utils/request";

export const getImageClassList = (page) => {
  return request("/admin/image_class/" + page, "get");
};

export const createImageClass = (data) => {
  return request("/admin/image_class", "post", data);
};

export const updateImageClass = (id, data) => {
  return request("/admin/image_class/" + id, "post", data);
};

export const deleteImageClass = (id) => {
  return request(`/admin/image_class/${id}/delete`, "post");
};
