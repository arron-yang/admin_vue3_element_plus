/*
 * @Author: yangy
 * @Date: 2024-01-02 16:30:06
 * @LastEditors: yangy aidreams@sina.com
 * @LastEditTime: 2024-02-26 16:04:43
 * @FilePath: /admin_vue3_element_plus/src/api/image.js
 * @Description:
 *
 * Copyright (c) 2024 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import request from "@/utils/request";

export const getImageList = (id, page = 1) => {
  return request(`/admin/image_class/${id}/image/${page}`, "get");
};

export const updateImage = (id, name) => {
  return request(`/admin/image/${id}`, "post", { name });
};

export const deleteImage = (ids) => {
  return request("/admin/image/delete_all", "post", { ids });
};

export const uploadImageAction = "/api/admin/image/upload"