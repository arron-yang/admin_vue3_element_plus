/*
 * @Author: yangy
 * @Date: 2023-12-25 11:47:57
 * @LastEditors: yangy
 * @LastEditTime: 2024-01-02 16:30:44
 * @FilePath: /admin_vue3_element_plus/src/api/index.js
 * @Description:
 *
 * Copyright (c) 2024 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
/*
 * @Author: yangy
 * @Date: 2023-12-25 11:47:57
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-26 14:51:58
 * @FilePath: /admin_vue3_element_plus/src/api/index.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import request from "@/utils/request";

export const getStatistic1 = () => {
  return request("/admin/statistics1", "get");
};

export const getStatistic2 = (type) => {
  return request("/admin/statistics2", "get");
};

export const getStatistic3 = (type) => {
  return request("/admin/statistics3?type=" + type, "get");
};
