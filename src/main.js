/*
 * @Author: yangy
 * @Date: 2023-12-12 12:22:56
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-26 16:02:30
 * @FilePath: /admin_vue3_element_plus/src/main.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import { router } from "./router";
import App from "./App.vue";
import "virtual:windi.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import store from "./store";
import "@/utils/permission";
import "nprogress/nprogress.css";
// 自定义指令
import permission from "@/directives/permission";
const app = createApp(App);
app.use(router);
app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(store);
app.use(permission);
app.mount("#app");
