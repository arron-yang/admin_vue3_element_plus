/*
 * @Author: yangy
 * @Date: 2023-12-26 15:59:59
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-26 16:20:18
 * @FilePath: /admin_vue3_element_plus/src/directives/permission.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */

import store from "@/store";

function hasPermission(value, el = false) {
  if (!Array.isArray(value)) {
    throw new Error(`需要配置权限,例如v-permission="['getStatistics3,GET']"`);
  }

  // != -1 表示查到了, 有这个权限
  const hasAuth =
    value.findIndex((v) => store.state.ruleNames.includes(v)) != -1;

  // 如果 el传过来不是false表示有这个节点,并且没有这个权限,就要把这个节点移除
  // 如果有父节点 就把这个父节点移除
  if (el && !hasAuth) {
    el.parentNode && el.parentNode.removeChild(el);
  }

  return hasAuth;
}

export default {
  install(app) {
    app.directive("permission", {
      mounted(el, binding) {
        // 数组的一个值
        // console.log(binding.value);
        hasPermission(binding.value, el);
      },
    });
    // console.log(app);
  },
};
