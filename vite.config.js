/*
 * @Author: yangy
 * @Date: 2023-12-12 12:22:56
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-14 11:34:19
 * @FilePath: /admin_vue3_element_plus/vite.config.js
 * @Description:
 *
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved.
 */
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import WindiCSS from "vite-plugin-windicss";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), WindiCSS()],
  resolve: {
    alias: {
      "@": path.resolve("./src"), // @代替src
    },
  },
  // 配置跨域代理
  server: {
    hmr: true, // ????
    proxy: {
      "/api": {
        target: "http://ceshi13.dishait.cn",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
