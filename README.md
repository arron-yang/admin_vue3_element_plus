<!--
 * @Author: yangy
 * @Date: 2023-12-12 12:22:56
 * @LastEditors: yangy
 * @LastEditTime: 2023-12-14 10:57:30
 * @FilePath: /admin_vue3_element_plus/README.md
 * @Description: 
 * 
 * Copyright (c) 2023 by 青柠利合科技(北京)有限公司, All Rights Reserved. 
-->
### 后台管理系统的api接口文档
http://dishaxy.dishait.cn/shopadminapi/

### 配置vite跨域
在vite.config.js文件中 增加serve配置项
